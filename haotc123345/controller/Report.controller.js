sap.ui.define([
    './reportstyle',
    './formatter',
    'sap/m/MessageToast'
], function (reportstyle, formatter, MessageToast) {
    "use strict";

    return {
        /**
         * Report template
         */
        _pdfContent_VN_EN: function (oReportInfo) {
            return {
                pageSize: 'A4',
                pageOrientation: 'landscape',
                content: [
                    {
                        text: [
                            'Tên công ty/ ',
                            { text: 'Company : ', italics: true },
                            oReportInfo.CompanyName
                        ],
                        style: 'header_contact_left'
                    },
                    {
                        text: [
                            'Địa chỉ công ty/ ',
                            { text: 'Address : ', italics: true },
                            oReportInfo.CompanyAddress
                        ],
                        style: 'header_contact_left'
                    },
                    { text: ' ' },
                    {
                        text: 'SỔ TIỀN GỬI NGÂN HÀNG', style: 'header'
                    },
                    {
                        text: 'CASH IN BANK BOOK', style: 'header', italics: true
                    },
                    {
                        text: [
                            "Từ ngày",
                            { text: "(From date) ", italics: true },
                            oReportInfo.FromPeriod,
                            " Đến ngày ",
                            { text: "(To date) ", italics: true },
                            oReportInfo.ToPeriod
                        ],
                        style: 'sub_header'
                    },
                    {
                        text: [
                            'Tài khoản ',
                            { text: '(Account): ', italics: true },
                            oReportInfo.Account
                        ],
                        style: 'sub_header'
                    },
                    {
                        text: [
                            'Nơi mở tài khoản giao dịch ',
                            { text: '(Account opening branch): ', italics: true },
                            oReportInfo.Account_branch
                        ],
                        style: 'sub_header'
                    },
                    {
                        text: [
                            'Số hiệu tài khoản tại nơi gửi ',
                            { text: '(Account No. at branch): ', italics: true },
                            oReportInfo.Account_no_branch
                        ],
                        style: 'sub_header'
                    },
                    {
                        text: [
                            'Loại tiền ',
                            { text: '(Currency): ', italics: true },
                            oReportInfo.Currency
                        ],
                        style: 'sub_header'
                    },
                    { text: ' ' },
                    {
                        table:
                        {
                            widths: [45, 45, 50, 150, 50, 50, 50, 40, 50, 50, 50, 35],
                            body: [
                                [
                                    {
                                        text:
                                            [
                                                'Ngày tháng ghi sổ\n',
                                                { text: '(Posting Date)', italics: true }
                                            ],
                                        rowSpan: 2,
                                        style: 'tableHeader_Margin10'
                                    },
                                    {
                                        text:
                                            [
                                                'Ngày tháng chứng từ\n',
                                                { text: '(Document Date)', italics: true }
                                            ],
                                        rowSpan: 2,
                                        style: 'tableHeader_Margin10'
                                    },
                                    {
                                        text:
                                            [
                                                'Số hiệu chứng từ\n',
                                                { text: '(Document Number)', italics: true }
                                            ],
                                        rowSpan: 2,
                                        style: 'tableHeader_Margin10'
                                    },
                                    {
                                        text:
                                            [
                                                'Diễn giải\n',
                                                { text: '(Description)', italics: true }
                                            ],
                                        rowSpan: 2,
                                        style: 'tableHeader_Margin10'
                                    },
                                    {
                                        text:
                                            [
                                                'Tài khoản đối ứng\n',
                                                { text: '(Offseting account)', italics: true }
                                            ],
                                        rowSpan: 2,
                                        style: 'tableHeader_Margin10'
                                    },
                                    {
                                        text:
                                            [
                                                'Số tiền NT\n',
                                                { text: '(Amount in Foreign currency)', italics: true }
                                            ],
                                        colSpan: 2,
                                        style: 'tableHeader'
                                    }, {},
                                    {
                                        text:
                                            [
                                                'Đơn vị tiền tệ\n',
                                                { text: '(Currency)', italics: true }
                                            ],
                                        rowSpan: 2,
                                        style: 'tableHeader_Margin10'
                                    },
                                    {
                                        text:
                                            [
                                                'Tỷ giá\n',
                                                { text: '(Exchange Rate)', italics: true }
                                            ],
                                        rowSpan: 2,
                                        style: 'tableHeader_Margin10'
                                    },
                                    {
                                        text:
                                            [
                                                'Số tiền\n',
                                                { text: '(Amount in local currency)', italics: true }
                                            ],
                                        colSpan: 2,
                                        style: 'tableHeader'
                                    }, {},
                                    {
                                        text:
                                            [
                                                'Đơn vị nội tệ\n',
                                                { text: '(Local Currency)', italics: true }
                                            ],
                                        rowSpan: 2,
                                        style: 'tableHeader_Margin10'
                                    },
                                ],
                                [
                                    {}, {}, {}, {}, {},
                                    {
                                        text: [
                                            'Thu\n',
                                            { text: '(Receipt)', italics: true }
                                        ],
                                        style: 'tableHeader'
                                    },
                                    {
                                        text: [
                                            'Chi\n',
                                            { text: '(Payment)', italics: true }
                                        ],
                                        style: 'tableHeader'
                                    }, {}, {},
                                    {
                                        text: [
                                            'Thu\n',
                                            { text: '(Receipt)', italics: true }
                                        ],
                                        style: 'tableHeader'
                                    },
                                    {
                                        text: [
                                            'Chi\n',
                                            { text: '(Payment)', italics: true }
                                        ],
                                        style: 'tableHeader'
                                    }, {}
                                ],
                                [
                                    { text: '1', style: 'tableHeader' },
                                    { text: '2', style: 'tableHeader' },
                                    { text: '3', style: 'tableHeader' },
                                    { text: '4', style: 'tableHeader' },
                                    { text: '5', style: 'tableHeader' },
                                    { text: '6', style: 'tableHeader' },
                                    { text: '7', style: 'tableHeader' },
                                    { text: '8', style: 'tableHeader' },
                                    { text: '9', style: 'tableHeader' },
                                    { text: '10', style: 'tableHeader' },
                                    { text: '11', style: 'tableHeader' },
                                    { text: '12', style: 'tableHeader' },
                                ]
                            ]
                        }
                    },
                    { text: ' ' },
                    {
                        columns: [
                            { text: ' ' },
                            {
                                width: 250,
                                text: [
                                    'Ngày',
                                    { text: "(Date) ", italics: true },
                                    ((oReportInfo.CreateReportDate.getUTCDate() / 10) < 1) ? "0" + oReportInfo.CreateReportDate.getUTCDate() : oReportInfo.CreateReportDate.getUTCDate(),
                                    ' Tháng',
                                    { text: "(Month) ", italics: true },
                                    (((oReportInfo.CreateReportDate.getUTCMonth() + 1) / 10) < 1) ? "0" + (oReportInfo.CreateReportDate.getUTCMonth() + 1) : (oReportInfo.CreateReportDate.getUTCMonth() + 1),
                                    ' Năm',
                                    { text: "(Year) ", italics: true },
                                    oReportInfo.CreateReportDate.getUTCFullYear()
                                ]
                            }
                        ],
                        style: 'sign_date'
                    },
                    { text: ' ' },
                    {
                        columns: [
                            {
                                width: 250,
                                text: [
                                    'Người ghi sổ\n',
                                    { text: '(Prepared by)', italics: true }
                                ],
                                style: 'sign_middle'
                            },
                            {
                                width: 275,
                                text: [
                                    'Kế toán trưởng\n',
                                    { text: '(Director)', italics: true }
                                ],
                                style: 'sign_middle'
                            },
                            {
                                width: 225,
                                text: [
                                    'Giám đốc\n',
                                    { text: '(Chief of accountant)', italics: true }
                                ],
                                style: 'sign_middle'
                            }
                        ]
                    },
                    {
                        columns: [
                            {
                                width: 250,
                                text: [
                                    '(Ký, họ tên)\n',
                                    { text: '(Sign, full name)', italics: true }
                                ],
                                style: 'sign_down'
                            },
                            {
                                width: 275,
                                text: [
                                    '(Ký, họ tên)\n',
                                    { text: '(Sign, full name)', italics: true }
                                ],
                                style: 'sign_down'
                            },
                            {
                                width: 225,
                                text: [
                                    '(Ký, họ tên)\n',
                                    { text: '(Sign, full name)', italics: true }
                                ],
                                style: 'sign_down'
                            }
                        ]
                    }
                ],
                styles: {
                    header_contact_left: {
                        bold: true,
                        fontSize: 8
                    },
                    header: {
                        fontSize: 12,
                        bold: true,
                        alignment: 'center'
                    },
                    sub_header: {
                        fontSize: 10,
                        alignment: 'center'
                    },
                    tableHeader: {
                        fontSize: 8,
                        color: 'black',
                        bold: true,
                        alignment: 'center'
                    },
                    tableHeader_Margin10: {
                        margin: [0, 10, 0, 0],
                        fontSize: 8,
                        color: 'black',
                        bold: true,
                        alignment: 'center'
                    },
                    tableData_Left: {
                        fontSize: 8,
                        alignment: 'left'
                    },
                    tableData_Center: {
                        fontSize: 8,
                        alignment: 'center'
                    },
                    tableData_Right: {
                        fontSize: 8,
                        alignment: 'right'
                    },
                    sign_date: {
                        fontSize: 8,
                        alignment: 'center'
                    },
                    sign_middle: {
                        fontSize: 8,
                        bold: true,
                        alignment: 'center'
                    },
                    sign_down: {
                        fontSize: 8,
                        italics: true,
                        alignment: 'center'
                    },
                    margin: {
                        margin: [0, 10, 0, 0],
                    }
                }
            };
        },

        _buildTableDataPDF: function (aData) {
            var aTableData = [];
            if (!aData.length) {
                return aTableData;
            }
            var obj = {};
            var arrCurrency = []
            for (var i = 0; i < aData.length; i++) {
                if (aData[i].doccurr !== "VND") {
                    obj[aData[i].doccurr] = aData[i];
                }
            }
            for (var key in obj) {
                arrCurrency.push(obj[key]);
            }

            aTableData.push([
                { text: '' },
                { text: '' },
                { text: '' },
                {
                    text: [
                        'Số dư đầu kỳ ',
                        { text: '(Opening balance)', italics: true }
                    ],
                    style: 'tableData_Left', bold: true
                },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: (aData[0].obalc != 0) ? formatter.formatMoneyInteger(aData[0].obalc) : '', bold: true, style: 'tableData_Right' },
                { text: (aData[0].obald != 0) ? formatter.formatMoneyInteger(aData[0].obald) : '', bold: true, style: 'tableData_Right' },
                { text: (aData[0].obalc != 0 || aData[0].obald != 0) ? aData[0].lcurr : '', bold: true, style: 'tableData_Center' }
            ]);

            aTableData.push([
                { text: '' },
                { text: '' },
                { text: '' },
                {
                    text: [
                        'Số phát sinh trong kỳ ',
                        { text: '(Amount incurring)', italics: true }
                    ],
                    style: 'tableData_Left', bold: true
                },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' }
            ]);

            aData.forEach((data, index) => {
                if (data.budat === null) {
                    aTableData.push([
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' },
                        { text: '', style: 'margin' }
                    ]);
                } else {
                    aTableData.push([
                        { text: formatter.formatDate1(data.budat), style: 'tableData_Left' },
                        { text: formatter.formatDate1(data.bldat), style: 'tableData_Left' },
                        { text: data.belnr, style: 'tableData_Center' },
                        { text: data.sgtxtfull, style: 'tableData_Center' },
                        { text: data.racct, style: 'tableData_Center' },
                        { text: data.doccurr != 'VND' ? ((data.inpaymentdc != 0) ? formatter.numberUnit(data.inpaymentdc) : '') : '', style: 'tableData_Right' },
                        { text: data.doccurr != 'VND' ? ((data.outpaymentdc != 0) ? formatter.numberUnit(data.outpaymentdc) : '') : '', style: 'tableData_Right' },
                        { text: data.doccurr != 'VND' ? data.doccurr : '', style: 'tableData_Center' },
                        { text: data.doccurr != 'VND' ? ((data.exrate != 0) ? formatter.formatMoneyInteger(data.exrate) : '') : '', style: 'tableData_Right' },
                        { text: (data.inpaymentlc != 0) ? formatter.formatMoneyInteger(data.inpaymentlc) : '', style: 'tableData_Right' },
                        { text: (data.outpaymentlc != 0) ? formatter.formatMoneyInteger(data.outpaymentlc) : '', style: 'tableData_Right' },
                        { text: (data.inpaymentlc != 0 || data.outpaymentlc != 0) ? aData[0].lcurr : '', style: 'tableData_Center' }
                    ]);
                }
            });

            for (var i = 0; i < arrCurrency.length; i++) {
                if (i === 0) {
                    aTableData.push([
                        { text: '' },
                        { text: '' },
                        { text: '' },
                        {
                            text: [
                                'Tổng số phát sinh trong kỳ ',
                                { text: '(Net change)', italics: true }
                            ],
                            style: 'tableData_Left', bold: true
                        },
                        { text: '' },
                        { text: (arrCurrency[i].amounta != 0) ? formatter.numberUnit(arrCurrency[i].amounta) : '', bold: true, style: 'tableData_Right' },
                        { text: (arrCurrency[i].amountb != 0) ? formatter.numberUnit(arrCurrency[i].amountb) : '', bold: true, style: 'tableData_Right' },
                        { text: (arrCurrency[i].amounta != 0 || arrCurrency[i].amountb != 0) ? aData[0].doccurr : '', bold: true, style: 'tableData_Center' },
                        { text: '' },
                        { text: (arrCurrency[i].amountc != 0) ? formatter.formatMoneyInteger(arrCurrency[i].amountc) : '', bold: true, style: 'tableData_Right' },
                        { text: (arrCurrency[i].amountd != 0) ? formatter.formatMoneyInteger(arrCurrency[i].amountd) : '', bold: true, style: 'tableData_Right' },
                        { text: (arrCurrency[i].amountc != 0 || arrCurrency[i].amountd != 0) ? aData[0].lcurr : '', bold: true, style: 'tableData_Center' }
                    ]);
                } else {
                    aTableData.push([
                        { text: '' },
                        { text: '' },
                        { text: '' },
                        { text: '' },
                        { text: '' },
                        { text: (arrCurrency[i].amounta != 0) ? formatter.numberUnit(arrCurrency[i].amounta) : '', bold: true, style: 'tableData_Right' },
                        { text: (arrCurrency[i].amountb != 0) ? formatter.numberUnit(arrCurrency[i].amountb) : '', bold: true, style: 'tableData_Right' },
                        { text: (arrCurrency[i].amounta != 0 || arrCurrency[i].amountb != 0) ? arrCurrency[i].doccurr : '', bold: true, style: 'tableData_Center' },
                        { text: '' },
                        { text: (arrCurrency[i].amountc != 0) ? formatter.formatMoneyInteger(arrCurrency[i].amountc) : '', bold: true, style: 'tableData_Right' },
                        { text: (arrCurrency[i].amountd != 0) ? formatter.formatMoneyInteger(arrCurrency[i].amountd) : '', bold: true, style: 'tableData_Right' },
                        { text: (arrCurrency[i].amountc != 0 || arrCurrency[i].amountd != 0) ? aData[0].lcurr : '', bold: true, style: 'tableData_Center' }
                    ]);
                }
            }

            aTableData.push([
                { text: '' },
                { text: '' },
                { text: '' },
                {
                    text: [
                        'Số dư cuối kỳ ',
                        { text: '(Closing balance)', italics: true }
                    ],
                    style: 'tableData_Left', bold: true
                },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: (aData[0].closingc != 0) ? formatter.formatMoneyInteger(aData[0].closingc) : '', bold: true, style: 'tableData_Right' },
                { text: (aData[0].closingd != 0) ? formatter.formatMoneyInteger(aData[0].closingd) : '', bold: true, style: 'tableData_Right' },
                { text: (aData[0].closingc != 0 || aData[0].closingd != 0) ? aData[0].lcurr : '', bold: true, style: 'tableData_Center' }
            ]);

            return aTableData;
        },

        _getDate: function (sDate) {
            var aDate = [];
            var aSplitData = sDate.split(" ");
            aSplitData.forEach(sDataSplit => {
                let aTempSplitData = sDataSplit.split("'");
                aTempSplitData.forEach(sTempDataSplit => {
                    if (Number(sTempDataSplit)) {
                        aDate.push(sTempDataSplit);
                    }
                })
            })
            return aDate;
        },

        /**
         * VietNamese report preview
         */
        onDataPriewPDF: function (oEvent, currentObject, oParamDatas) {
            var that = this;
            oParamDatas.p_budat_low = new Date(Date.UTC(oParamDatas.p_budat_low.getFullYear(),
                oParamDatas.p_budat_low.getMonth(),
                oParamDatas.p_budat_low.getDate()));
            var sPath = currentObject.getView().getModel().createKey("/ZVAS_ZCM302_2_V", {
                p_bukrs: oParamDatas.p_bukrs,
                p_gjahr: oParamDatas.p_gjahr,
                p_budat_low: oParamDatas.p_budat_low,
                s_budat: oParamDatas.s_budat,
                s_racct: oParamDatas.s_racct,
                s_lokkt: oParamDatas.s_lokkt,
                rd_glacc: oParamDatas.rd_glacc,
                rd_altacc: oParamDatas.rd_altacc
            });
            var aDate = that._getDate(oParamDatas.s_budat);
            currentObject.getView().getModel().read(sPath + "/Set", {
                success: function (oData) {
                    var pdf_Vietnamese;
                    var aData = oData.results;
                    var aTableData = [];
                    var aReportContent = null;
                    if (aData.length === 0) {
                        sap.ui.core.BusyIndicator.hide();
                        MessageToast.show("No Data");
                        return;
                    };
                    var oReportInfo = {
                        CompanyName: (aData.length) ? aData[0].compname : "",
                        CompanyAddress: (aData.length) ? aData[0].compaddr : "",
                        FromPeriod: formatter.formatDate2(aDate[0]),
                        ToPeriod: aDate[1] ? formatter.formatDate2(aDate[1]) : formatter.formatDate2(aDate[0]),
                        Account: (aData.length) ? aData[0].glheader : "",
                        Account_branch: (aData.length) ? aData[0].saknr : "",
                        Account_no_branch: (aData.length) ? aData[0].bankn : "",
                        Currency: (aData.length) ? aData[0].waersh : "",
                        CreateReportDate: new Date()
                    };
                    reportstyle._sortArrayData(aData);
                    pdf_Vietnamese = that._pdfContent_VN_EN(oReportInfo);
                    aReportContent = pdf_Vietnamese.content;
                    aTableData = that._buildTableDataPDF(aData);
                    aReportContent.forEach(content => {
                        if (content.table) {
                            content.table.body = content.table.body.concat(aTableData);
                            return;
                        }
                    });
                    that.oPdf_Download = pdf_Vietnamese;
                    pdfMake.createPdf(pdf_Vietnamese).getDataUrl(function (outDoc) {
                        var options = options || { scale: 1 };
                        function renderPage(page) {
                            var viewport = page.getViewport(options.scale);
                            var wrapper = document.createElement("div");
                            wrapper.className = "canvas-wrapper";
                            var canvas = document.createElement('canvas');
                            var ctx = canvas.getContext('2d');
                            var renderContext = {
                                canvasContext: ctx,
                                viewport: viewport
                            };
                            canvas.height = viewport.height;
                            canvas.width = viewport.width;
                            wrapper.appendChild(canvas)
                            document.getElementById('canvas_PDF').appendChild(wrapper);
                            page.render(renderContext);
                        }
                        function renderPages(pdfDoc) {
                            var oCanvasLayout = document.getElementById('canvas_PDF');
                            while (oCanvasLayout.hasChildNodes()) {
                                oCanvasLayout.removeChild(oCanvasLayout.childNodes[0]);
                            }
                            for (var num = 1; num <= pdfDoc.numPages; num++)
                                pdfDoc.getPage(num).then(renderPage);
                        }
                        PDFJS.disableWorker = true;
                        PDFJS.getDocument(outDoc).then(renderPages);
                    });
                    sap.ui.core.BusyIndicator.hide();
                },
                error: function (oError) {
                    formatter.httpRequest(oError);
                }
            });
        },

        /**
         *  Export PDF VietNamese report
         */
        onDataExportPDF_VN_EN: function (oEvent, currentObject, oParamDatas) {
            var that = this;
            oParamDatas.p_budat_low = new Date(Date.UTC(oParamDatas.p_budat_low.getFullYear(),
                oParamDatas.p_budat_low.getMonth(),
                oParamDatas.p_budat_low.getDate()));
            var sPath = currentObject.getView().getModel().createKey("/ZVAS_ZCM302_2_V", {
                p_bukrs: oParamDatas.p_bukrs,
                p_gjahr: oParamDatas.p_gjahr,
                p_budat_low: oParamDatas.p_budat_low,
                s_budat: oParamDatas.s_budat,
                s_racct: oParamDatas.s_racct,
                s_lokkt: oParamDatas.s_lokkt,
                rd_glacc: oParamDatas.rd_glacc,
                rd_altacc: oParamDatas.rd_altacc
            });
            var aDate = that._getDate(oParamDatas.s_budat);
            currentObject.getView().getModel().read(sPath + "/Set", {
                success: function (oData) {
                    var pdf_Vietnamese;
                    var aData = oData.results;
                    var aTableData = [];
                    var aReportContent = null;
                    if (aData.length === 0) {
                        sap.ui.core.BusyIndicator.hide();
                        MessageToast.show("No Data");
                        return;
                    };
                    var oReportInfo = {
                        CompanyName: (aData.length) ? aData[0].compname : "",
                        CompanyAddress: (aData.length) ? aData[0].compaddr : "",
                        FromPeriod: formatter.formatDate2(aDate[0]),
                        ToPeriod: aDate[1] ? formatter.formatDate2(aDate[1]) : formatter.formatDate2(aDate[0]),
                        Account: (aData.length) ? aData[0].glheader : "",
                        Account_branch: (aData.length) ? aData[0].saknr : "",
                        Account_no_branch: (aData.length) ? aData[0].bankn : "",
                        Currency: (aData.length) ? aData[0].waersh : "",
                        CreateReportDate: new Date()
                    };
                    reportstyle._sortArrayData(aData);
                    pdf_Vietnamese = that._pdfContent_VN_EN(oReportInfo);
                    aReportContent = pdf_Vietnamese.content;
                    aTableData = that._buildTableDataPDF(aData);
                    aReportContent.forEach(content => {
                        if (content.table) {
                            content.table.body = content.table.body.concat(aTableData);
                            return;
                        }
                    });
                    pdfMake.createPdf(pdf_Vietnamese).download('ZCM302.pdf');
                    sap.ui.core.BusyIndicator.hide();
                },
                error: function (oError) {
                    formatter.httpRequest(oError);
                }
            });
        },

        /**
         * Export excel VietNamese report
         */
        onDataExportExcel_VN_EN: function (oEvent, currentObject, oParamDatas) {
            var that = this;
            oParamDatas.p_budat_low = new Date(Date.UTC(oParamDatas.p_budat_low.getFullYear(),
                oParamDatas.p_budat_low.getMonth(),
                oParamDatas.p_budat_low.getDate()));
            var sPath = currentObject.getView().getModel().createKey("/ZVAS_ZCM302_2_V", {
                p_bukrs: oParamDatas.p_bukrs,
                p_gjahr: oParamDatas.p_gjahr,
                p_budat_low: oParamDatas.p_budat_low,
                s_budat: oParamDatas.s_budat,
                s_racct: oParamDatas.s_racct,
                s_lokkt: oParamDatas.s_lokkt,
                rd_glacc: oParamDatas.rd_glacc,
                rd_altacc: oParamDatas.rd_altacc
            });
            var aDate = that._getDate(oParamDatas.s_budat);
            currentObject.getView().getModel().read(sPath + "/Set", {
                success: function (oData) {
                    var aData = oData.results;
                    reportstyle._sortArrayData(aData);
                    if (aData.length === 0) {
                        sap.ui.core.BusyIndicator.hide();
                        MessageToast.show("No Data");
                        return;
                    };
                    var oReportInfo = {
                        CompanyName: (aData.length) ? aData[0].compname : "",
                        CompanyAddress: (aData.length) ? aData[0].compaddr : "",
                        FromPeriod: formatter.formatDate2(aDate[0]),
                        ToPeriod: aDate[1] ? formatter.formatDate2(aDate[1]) : formatter.formatDate2(aDate[0]),
                        Account: (aData.length) ? aData[0].glheader : "",
                        Account_branch: (aData.length) ? aData[0].saknr : "",
                        Account_no_branch: (aData.length) ? aData[0].bankn : "",
                        Currency: (aData.length) ? aData[0].waersh : "",
                        CreateReportDate: new Date()
                    };
                    var obj = {};
                    var arrCurrency = []
                    for (var i = 0; i < aData.length; i++) {
                        if (aData[i].doccurr !== "VND") {
                            obj[aData[i].doccurr] = aData[i];
                        }
                    }
                    for (var key in obj) {
                        arrCurrency.push(obj[key]);
                    }
                    var workbook = new ExcelJS.Workbook();
                    var worksheet;
                    var aRowValues = [];
                    var aMergeData = [];
                    var iRow = 1;
                    var aSettingStyle = [{
                        name: 'row', iPosition: `1`, oFont: { sName: 'Times New Roman', iSize: 10 }
                    }];
                    var oSettingCellSize = {
                        oSettingColumnWidth: {
                            'A': 10.5, 'B': 10.5, 'C': 15.5, 'D': 40.5, 'E': 10.5, 'F': 15.5,
                            'G': 15.5, 'H': 10.5, 'I': 10.5, 'J': 15.5, 'K': 15.5, 'L': 10.5
                        },
                        aSettingRowHeight: []
                    };

                    oSettingCellSize.aSettingRowHeight[12] = 38.5;
                    oSettingCellSize.aSettingRowHeight[13] = 38.5;
                    workbook.addWorksheet('My Sheet', {
                        views: [{ zoomScale: 100, showGridLines: false, style: 'pageBreakPreview' }],
                        pageSetup: { paperSize: 9, orientation: 'landscape' }
                    });
                    worksheet = workbook.getWorksheet('My Sheet');
                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Tên công ty/ ' },
                            { text: 'Company : ', font: { italic: true } },
                            { text: oReportInfo.CompanyName }
                        ]
                    }];
                    aMergeData.push(`A${iRow}:I${iRow}`);
                    aSettingStyle.push({ name: 'cell', sCellID: `A${iRow}`, oAlignment: { sVertical: 'bottom' }, oFont: { bBold: true } });
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Địa chỉ công ty/ ' },
                            { text: 'Address : ', font: { italic: true } },
                            { text: oReportInfo.CompanyAddress }
                        ]
                    }];
                    aMergeData.push(`A${iRow}:I${iRow}`);
                    aSettingStyle.push({ name: 'cell', sCellID: `A${iRow}`, oAlignment: { sVertical: 'bottom' }, oFont: { bBold: true } });
                    iRow++;

                    aRowValues[iRow] = [''];
                    iRow++;

                    aRowValues[iRow] = ['SỔ TIỀN GỬI NGÂN HÀNG'];
                    aMergeData.push(`A${iRow}:L${iRow}`);
                    aSettingStyle.push({
                        name: 'cell', sCellID: `A${iRow}`,
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center'
                        },
                        oFont: {
                            iSize: 16,
                            bBold: true
                        }
                    });
                    iRow++;

                    aRowValues[iRow] = ['BANK LEDGER'];
                    aMergeData.push(`A${iRow}:L${iRow}`);
                    aSettingStyle.push({
                        name: 'cell', sCellID: `A${iRow}`,
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center'
                        },
                        oFont: {
                            iSize: 16,
                            bBold: true
                        }
                    });
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Từ ngày ' },
                            { text: '(From date) ', font: { italic: true } },
                            { text: oReportInfo.FromPeriod },
                            { text: ' Đến ngày ' },
                            { text: '(To date) ', font: { italic: true } },
                            { text: oReportInfo.ToPeriod }
                        ]
                    }];
                    aMergeData.push(`A${iRow}:L${iRow}`);
                    aSettingStyle.push({
                        name: 'cell', sCellID: `A${iRow}`, oFont: { iSize: 12 },
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center'
                        },
                    });
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Tài khoản ' },
                            { text: '(Account) ', font: { italic: true } },
                            { text: oReportInfo.Account },
                        ]
                    }];
                    aMergeData.push(`A${iRow}:L${iRow}`);
                    aSettingStyle.push({
                        name: 'cell', sCellID: `A${iRow}`, oFont: { iSize: 12 },
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center'
                        },
                    });
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Nơi mở tài khoản giao dịch ' },
                            { text: '(Account opening branch) ', font: { italic: true } },
                            { text: oReportInfo.Account_branch },
                        ]
                    }];
                    aMergeData.push(`A${iRow}:L${iRow}`);
                    aSettingStyle.push({
                        name: 'cell', sCellID: `A${iRow}`, oFont: { iSize: 12 },
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center'
                        },
                    });
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Số hiệu tài khoản tại nơi gửi ' },
                            { text: '(Account No. at branch) ', font: { italic: true } },
                            { text: oReportInfo.Account_no_branch },
                        ]
                    }];
                    aMergeData.push(`A${iRow}:L${iRow}`);
                    aSettingStyle.push({
                        name: 'cell', sCellID: `A${iRow}`, oFont: { iSize: 12 },
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center'
                        },
                    });
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Loại tiền ' },
                            { text: '(Currency) ', font: { italic: true } },
                            { text: oReportInfo.Currency }
                        ]
                    }];
                    aMergeData.push(`A${iRow}:L${iRow}`);
                    aSettingStyle.push({
                        name: 'cell', sCellID: `A${iRow}`, oFont: { iSize: 12 },
                        oAlignment: {
                            sVertical: 'bottom',
                            sHorizontal: 'center'
                        },
                    });
                    iRow++;

                    aRowValues[iRow] = [''];
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Ngày tháng ghi sổ\n' },
                            { text: '(Posting Date)', font: { italic: true } }
                        ]
                    }];
                    aRowValues[iRow][1] = {
                        richText: [
                            { text: 'Ngày tháng chứng từ\n' },
                            { text: '(Document Date)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][2] = {
                        richText: [
                            { text: 'Số hiệu chứng từ\n' },
                            { text: '(Document Number)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][3] = {
                        richText: [
                            { text: 'Diễn giải\n' },
                            { text: '(Description)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][4] = {
                        richText: [
                            { text: 'Tài khoản đối ứng\n' },
                            { text: '(Business Partner)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][5] = {
                        richText: [
                            { text: 'Số tiền NT\n' },
                            { text: '(Amount in Foreign currency)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][7] = {
                        richText: [
                            { text: 'Loại tiền\n' },
                            { text: '(Currency)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][8] = {
                        richText: [
                            { text: 'Tỷ giá\n' },
                            { text: '(Exchange Rate)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][9] = {
                        richText: [
                            { text: 'Số tiền\n' },
                            { text: '(Amount in local currency)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][11] = {
                        richText: [
                            { text: 'Loại tiền\n' },
                            { text: '(Currency)', font: { italic: true } }
                        ]
                    };
                    aMergeData.push(`A${iRow}:A${iRow + 1}`);
                    aMergeData.push(`B${iRow}:B${iRow + 1}`);
                    aMergeData.push(`C${iRow}:C${iRow + 1}`);
                    aMergeData.push(`D${iRow}:D${iRow + 1}`);
                    aMergeData.push(`E${iRow}:E${iRow + 1}`);
                    aMergeData.push(`F${iRow}:G${iRow}`);
                    aMergeData.push(`H${iRow}:H${iRow + 1}`);
                    aMergeData.push(`I${iRow}:I${iRow + 1}`);
                    aMergeData.push(`J${iRow}:K${iRow}`);
                    aMergeData.push(`L${iRow}:L${iRow + 1}`);
                    aSettingStyle.push({
                        name: 'row', iPosition: iRow, oFont: { bBold: true },
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center',
                            bWrapText: true
                        },
                        oBorder: {
                            oLeft: { style: 'thin' },
                            oTop: { style: 'thin' },
                            oRight: { style: 'thin' },
                            oBottom: { style: 'thin' }
                        }
                    });
                    iRow++;

                    aRowValues[iRow] = [''];
                    aRowValues[iRow][5] = {
                        richText: [
                            { text: 'Thu\n' },
                            { text: '(Receipt)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][6] = {
                        richText: [
                            { text: 'Chi\n' },
                            { text: '(Payment)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][9] = {
                        richText: [
                            { text: 'Thu\n' },
                            { text: '(Receipt)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][10] = {
                        richText: [
                            { text: 'Chi\n' },
                            { text: '(Payment)', font: { italic: true } }
                        ]
                    };
                    aSettingStyle.push({
                        name: 'row', iPosition: iRow, oFont: { bBold: true },
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center',
                            bWrapText: true
                        },
                        oBorder: {
                            oLeft: { style: 'thin' },
                            oTop: { style: 'thin' },
                            oRight: { style: 'thin' },
                            oBottom: { style: 'thin' }
                        }
                    });
                    iRow++;

                    aRowValues[iRow] = ['1'];
                    aRowValues[iRow][1] = '2';
                    aRowValues[iRow][2] = '3';
                    aRowValues[iRow][3] = '4';
                    aRowValues[iRow][4] = '5';
                    aRowValues[iRow][5] = '6';
                    aRowValues[iRow][6] = '7';
                    aRowValues[iRow][7] = '8';
                    aRowValues[iRow][8] = '9';
                    aRowValues[iRow][9] = '10';
                    aRowValues[iRow][10] = '11';
                    aRowValues[iRow][11] = '12';
                    aSettingStyle.push({
                        name: 'row', iPosition: iRow, oFont: { bBold: true },
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center',
                            bWrapText: true
                        },
                        oBorder: {
                            oLeft: { style: 'thin' },
                            oTop: { style: 'thin' },
                            oRight: { style: 'thin' },
                            oBottom: { style: 'thin' }
                        }
                    });
                    iRow++;

                    aRowValues[iRow] = [''];
                    aRowValues[iRow][1] = '';
                    aRowValues[iRow][2] = '';
                    aRowValues[iRow][3] = {
                        richText: [
                            { text: 'Số dư đầu kỳ ' },
                            { text: '(Opening balance)', font: { italic: true } },
                        ]
                    };
                    aRowValues[iRow][4] = '';
                    aRowValues[iRow][5] = '';
                    aRowValues[iRow][6] = '';
                    aRowValues[iRow][7] = '';
                    aRowValues[iRow][8] = '';
                    aRowValues[iRow][9] = (aData[0].obalc != 0) ? parseFloat(aData[0].obalc) : '';
                    aRowValues[iRow][10] = (aData[0].obald != 0) ? parseFloat(aData[0].obald) : '';
                    aRowValues[iRow][11] = (aData[0].obalc != 0 || aData[0].obald != 0) ? aData[0].lcurr : '';
                    aSettingStyle.push({
                        name: 'row', iPosition: iRow, oFont: { bBold: true }, sNumberFormat: '#,##0',
                        oAlignment: {
                            sVertical: 'top',
                            sHorizontal: 'right',
                            bWrapText: true
                        },
                        oBorder: {
                            oLeft: { style: 'thin' },
                            oTop: { style: 'thin' },
                            oRight: { style: 'thin' },
                            oBottom: { style: 'thin' }
                        }
                    });
                    aSettingStyle.push({ name: 'cell', sCellID: `D${iRow}`, oAlignment: { sHorizontal: 'left' }, oFont: { bBold: true } });
                    aSettingStyle.push({ name: 'cell', sCellID: `H${iRow}`, oAlignment: { sHorizontal: 'center' }, oFont: { bBold: false } });
                    aSettingStyle.push({ name: 'cell', sCellID: `L${iRow}`, oAlignment: { sHorizontal: 'center' }, oFont: { bBold: false } });
                    iRow++;

                    aRowValues[iRow] = [''];
                    aRowValues[iRow][1] = '';
                    aRowValues[iRow][2] = '';
                    aRowValues[iRow][3] = {
                        richText: [
                            { text: 'Số phát sinh trong kỳ ' },
                            { text: '(Amount incurring)', font: { italic: true } },
                        ]
                    };
                    aRowValues[iRow][4] = '';
                    aRowValues[iRow][5] = '';
                    aRowValues[iRow][6] = '';
                    aRowValues[iRow][7] = '';
                    aRowValues[iRow][8] = '';
                    aRowValues[iRow][9] = '';
                    aRowValues[iRow][10] = '';
                    aRowValues[iRow][11] = '';
                    aSettingStyle.push({
                        name: 'row', iPosition: iRow, oFont: { bBold: true }, sNumberFormat: '#,##0',
                        oAlignment: {
                            sVertical: 'top',
                            sHorizontal: 'right',
                            bWrapText: true
                        },
                        oBorder: {
                            oLeft: { style: 'thin' },
                            oTop: { style: 'thin' },
                            oRight: { style: 'thin' },
                            oBottom: { style: 'thin' }
                        }
                    });
                    aSettingStyle.push({ name: 'cell', sCellID: `D${iRow}`, oAlignment: { sHorizontal: 'left' }, oFont: { bBold: true } });
                    iRow++;

                    aData.forEach((data, index) => {
                        if (data.budat === null) {
                            aRowValues[iRow] = [''];
                            aSettingStyle.push({
                                name: 'row', iPosition: iRow,
                                oBorder: {
                                    oLeft: { style: 'thin' },
                                    oTop: { style: 'thin' },
                                    oRight: { style: 'thin' },
                                    oBottom: { style: 'thin' }
                                }
                            });
                            iRow++;
                        } else {
                            aRowValues[iRow] = [formatter.formatDate1(data.budat)];
                            aRowValues[iRow][1] = formatter.formatDate1(data.bldat);
                            aRowValues[iRow][2] = data.belnr;
                            aRowValues[iRow][3] = data.sgtxtfull;
                            aRowValues[iRow][4] = data.racct;
                            aRowValues[iRow][5] = (data.doccurr != 'VND') ? ((data.inpaymentdc != 0) ? parseFloat(data.inpaymentdc) : '') : '';
                            aRowValues[iRow][6] = (data.doccurr != 'VND') ? ((data.outpaymentdc != 0) ? parseFloat(data.outpaymentdc) : '') : '';
                            aRowValues[iRow][7] = (data.doccurr != 'VND') ? ((data.inpaymentdc != 0 || data.outpaymentdc != 0) ? data.doccurr : '') : '';
                            aRowValues[iRow][8] = (data.doccurr != 'VND') ? ((data.exrate != 0) ? parseFloat(data.exrate) : '') : '';
                            aRowValues[iRow][9] = (data.inpaymentlc != 0) ? parseFloat(data.inpaymentlc) : '';
                            aRowValues[iRow][10] = (data.outpaymentlc != 0) ? parseFloat(data.outpaymentlc) : '';
                            aRowValues[iRow][11] = (data.inpaymentlc != 0 || data.outpaymentlc != 0) ? data.lcurr : '';
                            aSettingStyle.push({
                                name: 'row', iPosition: iRow, sNumberFormat: '#,##0',
                                oAlignment: {
                                    sVertical: 'top',
                                    sHorizontal: 'right',
                                    bWrapText: true
                                },
                                oBorder: {
                                    oLeft: { style: 'thin' },
                                    oTop: { style: 'thin' },
                                    oRight: { style: 'thin' },
                                    oBottom: { style: 'thin' }
                                }
                            });
                            aSettingStyle.push({ name: 'cell', sCellID: `A${iRow}`, sNumberFormat: '#,##0.00', oAlignment: { sHorizontal: 'left' } });
                            aSettingStyle.push({ name: 'cell', sCellID: `B${iRow}`, oAlignment: { sHorizontal: 'left' } });
                            aSettingStyle.push({ name: 'cell', sCellID: `F${iRow}`, sNumberFormat: '#,##0.00' });
                            aSettingStyle.push({ name: 'cell', sCellID: `G${iRow}`, sNumberFormat: '#,##0.00' });
                            aSettingStyle.push({ name: 'cell', sCellID: `H${iRow}`, oAlignment: { sHorizontal: 'center' }, oFont: { bBold: false } });
                            aSettingStyle.push({ name: 'cell', sCellID: `L${iRow}`, oAlignment: { sHorizontal: 'center' }, oFont: { bBold: false } });
                            iRow++;
                        }
                    });
                    for (var i = 0; i < arrCurrency.length; i++) {
                        aRowValues[iRow] = [''];
                        aRowValues[iRow][1] = '';
                        aRowValues[iRow][2] = '';
                        if (i === 0) {
                            aRowValues[iRow][3] = {
                                richText: [
                                    { text: 'Tổng số phát sinh trong kỳ ' },
                                    { text: '(Net change)', font: { italic: true } },
                                ]
                            };
                        } else {
                            aRowValues[iRow][3] = '';
                        }
                        aRowValues[iRow][4] = '';
                        aRowValues[iRow][5] = (arrCurrency[i].amounta != 0) ? parseFloat(arrCurrency[i].amounta) : '';
                        aRowValues[iRow][6] = (arrCurrency[i].amountb != 0) ? parseFloat(arrCurrency[i].amountb) : '';
                        aRowValues[iRow][7] = (arrCurrency.doccurr != 'VND') ? ((arrCurrency[i].amounta != 0 || arrCurrency[i].amountb != 0) ? arrCurrency.doccurr : '') : '';
                        aRowValues[iRow][8] = '';
                        aRowValues[iRow][9] = (arrCurrency[i].amountc != 0) ? parseFloat(arrCurrency[i].amountc) : '';
                        aRowValues[iRow][10] = (arrCurrency[i].amountd != 0) ? parseFloat(arrCurrency[i].amountd) : '';
                        aRowValues[iRow][11] = (arrCurrency[i].amountc != 0 || arrCurrency[i].amountd != 0) ? aData[0].lcurr : '';
                        aSettingStyle.push({
                            name: 'row', iPosition: iRow, oFont: { bBold: true }, sNumberFormat: '#,##0',
                            oAlignment: {
                                sVertical: 'top',
                                sHorizontal: 'right',
                                bWrapText: true
                            },
                            oBorder: {
                                oLeft: { style: 'thin' },
                                oTop: { style: 'thin' },
                                oRight: { style: 'thin' },
                                oBottom: { style: 'thin' }
                            }
                        });
                        aSettingStyle.push({ name: 'cell', sCellID: `D${iRow}`, oAlignment: { sHorizontal: 'left' }, oFont: { bBold: true } });
                        aSettingStyle.push({ name: 'cell', sCellID: `F${iRow}`, sNumberFormat: '#,##0.00' });
                        aSettingStyle.push({ name: 'cell', sCellID: `G${iRow}`, sNumberFormat: '#,##0.00' });
                        aSettingStyle.push({ name: 'cell', sCellID: `H${iRow}`, oAlignment: { sHorizontal: 'center' }, oFont: { bBold: false } });
                        aSettingStyle.push({ name: 'cell', sCellID: `L${iRow}`, oAlignment: { sHorizontal: 'center' }, oFont: { bBold: false } });
                        iRow++;
                    }
                    aRowValues[iRow] = [''];
                    aRowValues[iRow][1] = '';
                    aRowValues[iRow][2] = '';
                    aRowValues[iRow][3] = {
                        richText: [
                            { text: 'Số dư cuối kỳ ' },
                            { text: '(Closing balance)', font: { italic: true } },
                        ]
                    };
                    aRowValues[iRow][4] = '';
                    aRowValues[iRow][5] = '';
                    aRowValues[iRow][6] = '';
                    aRowValues[iRow][7] = '';
                    aRowValues[iRow][8] = '';
                    aRowValues[iRow][9] = (aData[0].closingc != 0) ? parseFloat(aData[0].closingc) : '';
                    aRowValues[iRow][10] = (aData[0].closingd != 0) ? parseFloat(aData[0].closingd) : '';
                    aRowValues[iRow][11] = (aData[0].closingc != 0 || aData[0].closingd != 0) ? aData[0].lcurr : '';
                    aSettingStyle.push({
                        name: 'row', iPosition: iRow, oFont: { bBold: true }, sNumberFormat: '#,##0',
                        oAlignment: {
                            sVertical: 'top',
                            sHorizontal: 'right',
                            bWrapText: true
                        },
                        oBorder: {
                            oLeft: { style: 'thin' },
                            oTop: { style: 'thin' },
                            oRight: { style: 'thin' },
                            oBottom: { style: 'thin' }
                        }
                    });
                    aSettingStyle.push({ name: 'cell', sCellID: `D${iRow}`, oAlignment: { sHorizontal: 'left' }, oFont: { bBold: true } });
                    aSettingStyle.push({ name: 'cell', sCellID: `H${iRow}`, oAlignment: { sHorizontal: 'center' }, oFont: { bBold: false } });
                    aSettingStyle.push({ name: 'cell', sCellID: `L${iRow}`, oAlignment: { sHorizontal: 'center' }, oFont: { bBold: false } });
                    iRow++;

                    aRowValues[iRow] = [];
                    iRow++;

                    aRowValues[iRow] = [''];
                    aRowValues[iRow][8] = {
                        richText: [
                            { text: 'Ngày' },
                            { text: '(Date) ', font: { italic: true } },
                            { text: ((oReportInfo.CreateReportDate.getUTCDate() / 10) < 1) ? "0" + oReportInfo.CreateReportDate.getUTCDate() : oReportInfo.CreateReportDate.getUTCDate() },
                            { text: ' Tháng' },
                            { text: '(Month) ', font: { italic: true } },
                            { text: (((oReportInfo.CreateReportDate.getUTCMonth() + 1) / 10) < 1) ? "0" + (oReportInfo.CreateReportDate.getUTCMonth() + 1) : oReportInfo.CreateReportDate.getUTCMonth() + 1 },
                            { text: ' Năm' },
                            { text: '(Year) ', font: { italic: true } },
                            { text: oReportInfo.CreateReportDate.getUTCFullYear() }
                        ]
                    };
                    aMergeData.push(`I${iRow}:L${iRow}`);
                    aSettingStyle.push({ name: 'cell', sCellID: `J${iRow}`, oAlignment: { sVertical: 'middle', sHorizontal: 'center', bWrapText: true } });
                    iRow++;

                    aRowValues[iRow] = [];
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: 'Người ghi sổ\n' },
                            { text: '(Prepared by)', font: { italic: true } },
                        ]
                    }];
                    aRowValues[iRow][4] = {
                        richText: [
                            { text: 'Kế toán trưởng\n' },
                            { text: '(Chief of accountant)', font: { italic: true } },
                        ]
                    };
                    aRowValues[iRow][8] = {
                        richText: [
                            { text: 'Giám đốc\n' },
                            { text: '(Director)', font: { italic: true } },
                        ]
                    };
                    aMergeData.push(`A${iRow}:C${iRow}`);
                    aMergeData.push(`I${iRow}:K${iRow}`);
                    aSettingStyle.push({
                        name: 'row', iPosition: iRow, oFont: { bBold: true },
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center',
                            bWrapText: true
                        },
                    });
                    oSettingCellSize.aSettingRowHeight[iRow] = 25.5;
                    iRow++;

                    aRowValues[iRow] = [{
                        richText: [
                            { text: '(Ký, họ tên)\n' },
                            { text: '(Sign, full name)', font: { italic: true } }
                        ]
                    }];
                    aRowValues[iRow][4] = {
                        richText: [
                            { text: '(Ký, họ tên)\n' },
                            { text: '(Sign, full name)', font: { italic: true } }
                        ]
                    };
                    aRowValues[iRow][8] = {
                        richText: [
                            { text: '(Ký, họ tên)\n' },
                            { text: '(Sign, full name)', font: { italic: true } }
                        ]
                    };
                    aMergeData.push(`A${iRow}:C${iRow}`);
                    aMergeData.push(`I${iRow}:K${iRow}`);
                    aSettingStyle.push({
                        name: 'row', iPosition: iRow,
                        oAlignment: {
                            sVertical: 'middle',
                            sHorizontal: 'center',
                            bWrapText: true
                        }
                    });
                    oSettingCellSize.aSettingRowHeight[iRow] = 34.5;

                    aSettingStyle[0].iPosition = aSettingStyle[0].iPosition.concat(`:${iRow}`);
                    worksheet.addRows(aRowValues);

                    // Format cell alignment and font
                    aSettingStyle.forEach(oSettingStyle => {
                        reportstyle.settingAlignment(worksheet, oSettingStyle);
                        reportstyle.settingFont(worksheet, oSettingStyle);
                        reportstyle.settingBorder(worksheet, oSettingStyle);
                        reportstyle.settingNumberFormat(worksheet, oSettingStyle);
                    });

                    // Merge cells
                    aMergeData.forEach(sMergeData => {
                        worksheet.mergeCells(sMergeData);
                    });

                    // Format cell size
                    for (let sPropertyName in oSettingCellSize) {
                        if (sPropertyName == 'oSettingColumnWidth') {
                            let oData = oSettingCellSize[sPropertyName];
                            for (let sColumnName in oData) {
                                worksheet.getColumn(sColumnName).width = oData[sColumnName];
                            }
                        } else {
                            let aData = oSettingCellSize[sPropertyName];
                            aData.forEach(iRowHeight => {
                                let iRowNumber = aData.indexOf(iRowHeight);
                                worksheet.getRow(iRowNumber).height = iRowHeight;
                                aData[iRowNumber] = null;
                            });
                        }
                    }
                    var sFileName = 'ZCM302.xlsx';
                    workbook.xlsx.writeBuffer().then((data) => {
                        let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                        window.saveAs(blob, sFileName);
                    });
                    sap.ui.core.BusyIndicator.hide();
                },
                error: function (oError) {
                    formatter.httpRequest(oError);
                }
            });
        },
    };
});