/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"zcm302/zcm302/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
