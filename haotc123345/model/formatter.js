sap.ui.define([], function () {
	"use strict";

	return {
		
		/**
		 * Rounds the number unit value to 2 digits
		 * @public
		 * @param {string} sValue the number string to be rounded
		 * @returns {string} sValue with 2 digits rounded
		 */
		numberUnit : function (sValue) {
			if (!sValue) {
				return "";
			}
			return parseFloat(sValue).toFixed(2);
		},

		formatMoneyInteger: function (sValue) {
			var sMoneyFormat = new Intl.NumberFormat('en-US').format(sValue);
			return sMoneyFormat;
		},

		formatMoneyFloat2Digit: function (sValue) {
			var sMoneyFormat = new Intl.NumberFormat('en-US',{
                maximumFractionDigits: 2,
                minimumFractionDigits: 2}
            ).format(sValue);
			return sMoneyFormat;
		},

        formatDate: function (sValue){
            var sdate = new Date(sValue),
            syear = sdate.getFullYear(),
            smonth = sdate.getMonth() + 1,
            sday = sdate.getDate();
            if (sday < 10) {
                sday = "0" + sday;
            }
            if (smonth < 10) {
                smonth = "0" + smonth;
            }
            var temp="" + syear + smonth + sday;
            return temp;
        },

		formatDate1: function (sValue) {
			var temp = this.formatDate(sValue);
			var syear = temp.slice(0, 4),
				smonth = temp.slice(4, 6),
				sday = temp.slice(6, 8),
				result = syear + "-" + smonth + "-" + sday;
			return result;
		},

		formatDate2: function (sValue) {
			var syear = sValue.slice(0, 4),
				smonth = sValue.slice(4, 6),
				sday = sValue.slice(6, 8),
				result = sday + "/" + smonth + "/" + syear;
			return result;
        },

		formatDate3_EN: function (sValue) {
			var temp = this.formatDate(sValue);
			var syear = temp.slice(0, 4),
				smonth = temp.slice(4, 6),
				sday = temp.slice(6, 8),
				result = "Date " + sday + " Month " + smonth + " Year " + syear;
			return result;
        },

		formatDate3_VN: function (sValue) {
			var temp = this.formatDate(sValue);
			var syear = temp.slice(0, 4),
				smonth = temp.slice(4, 6),
				sday = temp.slice(6, 8),
				result = "Ngày " + sday + " Tháng " + smonth + " Năm " + syear;
			return result;
        },

		formatDate3_VN_EN: function (sValue) {
			var temp = this.formatDate(sValue);
			var syear = temp.slice(0, 4),
				smonth = temp.slice(4, 6),
                sday = temp.slice(6, 8),
				result = "Ngày (day) " + sday + " tháng (month) " + smonth + " năm (year) " + syear;
			return result;
        }
	};

});