sap.ui.define([], function () {
	"use strict";

	return {
		aColumns : ['A','B','C','D','E','F','G','H','I','J','K','L'],
		
		_sortArrayData: function(aData){
			aData.sort(function(a, b) {
				var sRacctA = a.racct.toUpperCase(); // bỏ qua hoa thường
				var sRacctB = b.racct.toUpperCase(); // bỏ qua hoa thường
				if (sRacctA < sRacctB) {
				  return -1;
				}
				if (sRacctA > sRacctB) {
				  return 1;
				}
			
				// name trùng nhau
				return 0;
			  });
		},

		settingAlignment: function(oWorkSheet, oPosition) {
			if(!oPosition.oAlignment){
				return;
			}

			var oFontCustom,
			aColumnsTemp = this.aColumns.slice();
		
			switch(oPosition.name){
				case 'cell':
					oFontCustom  = oWorkSheet.getCell(oPosition.sCellID);
					oFontCustom.alignment ? oFontCustom.alignment : oFontCustom.alignment = {};
					oPosition.oAlignment.sVertical ? oFontCustom.alignment.vertical = oPosition.oAlignment.sVertical : '';
					oPosition.oAlignment.sHorizontal ? oFontCustom.alignment.horizontal = oPosition.oAlignment.sHorizontal : '';
					oPosition.oAlignment.bWrapText ? oFontCustom.alignment.wrapText = oPosition.oAlignment.bWrapText : '';
					break;
				case 'row':
					aColumnsTemp.forEach(sColumn => {
						oFontCustom  = oWorkSheet.getCell(sColumn + oPosition.iPosition);
						oFontCustom.alignment ? oFontCustom.alignment : oFontCustom.alignment = {};
						oPosition.oAlignment.sVertical ? oFontCustom.alignment.vertical = oPosition.oAlignment.sVertical : '';
						oPosition.oAlignment.sHorizontal ? oFontCustom.alignment.horizontal = oPosition.oAlignment.sHorizontal : '';
						oPosition.oAlignment.bWrapText ? oFontCustom.alignment.wrapText = oPosition.oAlignment.bWrapText : '';
					});
					break;
			}
		},
		
		settingBorder: function(oWorkSheet, oPosition) {
			if(!oPosition.oBorder){
				return;
			}
			
			var oBorderCustom;
			switch(oPosition.name){
				case 'cell':
					oBorderCustom  = oWorkSheet.getCell(oPosition.sCellID);
					oBorderCustom.border ? oBorderCustom.border : oBorderCustom.border = {};
					oPosition.oBorder.oLeft ? oBorderCustom.border.left = oPosition.oBorder.oLeft : '';
					oPosition.oBorder.oTop ? oBorderCustom.border.top = oPosition.oBorder.oTop : '';
					oPosition.oBorder.oRight ? oBorderCustom.border.right = oPosition.oBorder.oRight : '';
					oPosition.oBorder.oBottom ? oBorderCustom.border.bottom = oPosition.oBorder.oBottom : '';
					break;
				case 'row':
					this.aColumns.forEach(sColumn => {
						oBorderCustom  = oWorkSheet.getCell(sColumn + oPosition.iPosition);
						oBorderCustom.border ? oBorderCustom.border : oBorderCustom.border = {};
						oPosition.oBorder.oLeft ? oBorderCustom.border.left = oPosition.oBorder.oLeft : '';
						oPosition.oBorder.oTop ? oBorderCustom.border.top = oPosition.oBorder.oTop : '';
						oPosition.oBorder.oRight ? oBorderCustom.border.right = oPosition.oBorder.oRight : '';
						oPosition.oBorder.oBottom ? oBorderCustom.border.bottom = oPosition.oBorder.oBottom : '';
					});
					break;
			}
		},

		settingFill: function(oWorkSheet, oPosition) {
			if(!oPosition.oFill){
				return;
			}
			
			var oFillCustom;
			switch(oPosition.name){
				case 'cell':
					oFillCustom  = oWorkSheet.getCell(oPosition.sCellID);
					oFillCustom.fill  ? oFillCustom.fill  : oFillCustom.fill  = {type: 'pattern', pattern:'solid'};
					oPosition.oFill.sBgColor ? oFillCustom.fill.bgColor = { argb : oPosition.oFill.sBgColor } : '';
					oPosition.oFill.sFgColor ? oFillCustom.fill.fgColor = { argb : oPosition.oFill.sFgColor } : '';
					break;
				case 'row':
					this.aColumns.forEach(sColumn => {
						oFillCustom  = oWorkSheet.getCell(sColumn + oPosition.iPosition);
						oFillCustom.fill  ? oFillCustom.fill  : oFillCustom.fill  = {type: 'pattern', pattern:'solid'};
						oPosition.oFill.sBgColor ? oFillCustom.fill.bgColor = { argb : oPosition.oFill.sBgColor } : '';
						oPosition.oFill.sFgColor ? oFillCustom.fill.fgColor = { argb : oPosition.oFill.sFgColor } : '';
					});
					break;
			}
		},

		settingNumberFormat: function(oWorkSheet, oPosition) {
			if(!oPosition.sNumberFormat){
				return;
			}
			
			var oNumberFormatCustom;
			switch(oPosition.name){
				case 'cell':
					oNumberFormatCustom  = oWorkSheet.getCell(oPosition.sCellID);
					oNumberFormatCustom.numFmt = oPosition.sNumberFormat;
					break;
				case 'row':
					this.aColumns.forEach(sColumn => {
						oNumberFormatCustom  = oWorkSheet.getCell(sColumn + oPosition.iPosition);
						oNumberFormatCustom.numFmt = oPosition.sNumberFormat;
					});
					break;
			}
		},

		settingFont: function(oWorkSheet, oPosition) {
			if(!oPosition.oFont){
				return;
			}

			var oFontCustom,
				aRichText,
				aColumnsTemp = this.aColumns.slice();
				
			switch(oPosition.name){
				case 'cell':
					aRichText = oWorkSheet.getCell(oPosition.sCellID).value.richText;
					
					if(aRichText){
						aRichText.forEach( oText => {
							oFontCustom  = oText;
							oFontCustom.font ? oFontCustom.font : oFontCustom.font = {};
							oPosition.oFont.sName ? oFontCustom.font.name = oPosition.oFont.sName : '';
							oPosition.oFont.iSize ? oFontCustom.font.size = oPosition.oFont.iSize : '';
							oPosition.oFont.bBold ? oFontCustom.font.bold = oPosition.oFont.bBold : '';
							oPosition.oFont.bItalic ? oFontCustom.font.italic = oPosition.oFont.bItalic : '';
							oPosition.oFont.bUnderline ? oFontCustom.font.underline = oPosition.oFont.bUnderline : '';
						});
					} else {
						oFontCustom  = oWorkSheet.getCell(oPosition.sCellID);
						oFontCustom.font ? oFontCustom.font : oFontCustom.font = {};
						oPosition.oFont.sName ? oFontCustom.font.name = oPosition.oFont.sName : '';
						oPosition.oFont.iSize ? oFontCustom.font.size = oPosition.oFont.iSize : '';
						oPosition.oFont.bBold ? oFontCustom.font.bold = oPosition.oFont.bBold : '';
						oPosition.oFont.bItalic ? oFontCustom.font.italic = oPosition.oFont.bItalic : '';
						oPosition.oFont.bUnderline ? oFontCustom.font.underline = oPosition.oFont.bUnderline : '';
					}
					break;
				case 'row':
					let iStartCol,
						iEndCol;

					try{
						if(oPosition.iPosition.includes(':')){
							let aPositions = oPosition.iPosition.split(':');
							iStartCol = Number.parseInt(aPositions[0]);
							iEndCol = Number.parseInt(aPositions[1]);
						}
					} catch (e) {
						iStartCol = oPosition.iPosition;
						iEndCol = oPosition.iPosition;
					};

					for(let i = iStartCol; i <= iEndCol; i++) {
						aColumnsTemp.forEach(sColumn => {
							if(oWorkSheet.getCell(sColumn + i).value || oWorkSheet.getCell(sColumn + i).value == 0){
								aRichText = oWorkSheet.getCell(sColumn + i).value.richText;
								if(aRichText){
									aRichText.forEach( oText => {
										oFontCustom  = oText;
										oFontCustom.font ? oFontCustom.font : oFontCustom.font = {};
										oPosition.oFont.sName ? oFontCustom.font.name = oPosition.oFont.sName : '';
										oPosition.oFont.iSize ? oFontCustom.font.size = oPosition.oFont.iSize : '';
										oPosition.oFont.bBold ? oFontCustom.font.bold = oPosition.oFont.bBold : '';
										oPosition.oFont.bItalic ? oFontCustom.font.italic = oPosition.oFont.bItalic : '';
										oPosition.oFont.bUnderline ? oFontCustom.font.underline = oPosition.oFont.bUnderline : '';
									});
								} else {
									oFontCustom  = oWorkSheet.getCell(sColumn + i);
									oFontCustom.font ? oFontCustom.font : oFontCustom.font = {};
									oPosition.oFont.sName ? oFontCustom.font.name = oPosition.oFont.sName : '';
									oPosition.oFont.iSize ? oFontCustom.font.size = oPosition.oFont.iSize : '';
									oPosition.oFont.bBold ? oFontCustom.font.bold = oPosition.oFont.bBold : '';
									oPosition.oFont.bItalic ? oFontCustom.font.italic = oPosition.oFont.bItalic : '';
									oPosition.oFont.bUnderline ? oFontCustom.font.underline = oPosition.oFont.bUnderline : '';
								}
							}
						});
					}
					break;
			}
		}
    }

});